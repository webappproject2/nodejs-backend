import Express from "express"
import Path from "path"
import Logger from "morgan"
import CookieParser from "cookie-parser"
import BodyParser from "body-parser"
import passport from "passport"
import Config from "./src/config"
import Session from "express-session"
import helmet from "helmet"
import cors from "cors"
import {SignInStrategy, Serialize as PassportSerialize, Deserialize as PassportDeserialize} from "./src/lib/passport/passport"

const app = Express()

app.use(helmet())
app.use(cors({
  origin: "http://localhost:8080",
  optionsSuccessStatus: 200,
  "credentials": true
}))

// Logger
app.use(Logger("dev"))

// BodyParser
app.use(BodyParser.json())
app.use(BodyParser.urlencoded({ extended: true }))

// CookieParser
app.use(CookieParser())

// Express
app.use(Express.static(Path.join(__dirname, "public")))
app.set("port", process.env.PORT || 3000)
app.disable("x-powered-by")

// Session
app.use(Session({ secret: Config.session.secret, resave: true, saveUninitialized:true}))

// PassportJs
app.use(passport.initialize())
app.use(passport.session())
passport.use("local-signin", SignInStrategy)
passport.serializeUser(PassportSerialize)
passport.deserializeUser(PassportDeserialize)


// Routes
import Authentication from "./src/route/controller/Authentication"
import User from "./src/route/controller/UserManagement"
import Album from "./src/route/controller/Album"
import ImageRoute from "./src/route/controller/Image"

app.use("/api/", Authentication)
app.use("/api/user", User)
app.use("/api/album", Album)
app.use("/api/image", ImageRoute)


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error("Not Found")
  err.status = 404
  next(err)
})

// error handler
app.use(function(err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get("env") === "development" ? err : {}

  // render the error page
  res.status(err.status || 500)
})

// Start Server
app.listen(app.get("port"), function(){
  console.log("Listening on port:", app.get("port"))
})
