import bcrypt from "bcrypt"
const saltRounds = 9
// Tested

function generateHashPassword(plainText){
  return bcrypt.hash(plainText, saltRounds) // Returns Promise
}

function verifyPassword(plainText, hash){
  return bcrypt.compare(plainText, hash) // Returns Promise
}

export { generateHashPassword, verifyPassword }
