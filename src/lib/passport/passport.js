import {Strategy as LocalStrategy} from "passport-local"
import { verifyPassword } from "../crypto/crypto"
import {User} from "../../db/query"

const SignInStrategy = new LocalStrategy((username, password, done) => {
  // if err => done(err)
  // if !user => done(null, false)
  // if !user.verifyPassword =>  done(null, false)
  // return done(null, user)

  User.isUserExist(username, (exist) => {
    if (!exist) return done(null, false)
    else {
      User.getUserByUsernameExcludeAllExceptPassword(username, (data) => {
        verifyPassword(password, data.hashPassword).then((match) => {
          if (match) return done(null, {"username": data.username})
          else return done(null, false)
        }).catch((err) => {
          throw err
        })
      }, (err) => {
        return done(err)
      })
    }
  })
})

function Serialize(user, done){
  if (!user) return done(null, false)
  else {
    return done(null, user.username)
  }
}

function Deserialize(username, done){
  User.getUserByUsernameExcludeAllExceptId(username, (data) => {
    if (!data) {
      return done(new Error("Could not find user"), false)
    }
    else return done(null, data)
  }, (err) => {
    return done(err, false)
  })


}

export {SignInStrategy, Serialize, Deserialize}
