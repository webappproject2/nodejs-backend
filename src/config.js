export default {
  db: {
    name: "webapp-ImageAlbum",
    username: "root",
    password: "123456",
    connectionPool: {
      max: 1000,
      min: 0,
      idle: 10000
    },
    dialect: "mysql",
    host: "localhost",
    port: 3306,
    define: {
      engine: "MYISAM"
    }
  },
  session: {
    secret: "Lb+sbWH2iTSX8JH92NfqO983b1Cz4QGHMOf0xOChhG8="
  }
}
