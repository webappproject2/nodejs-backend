import { Tag, ImageTag } from "../model"
import {each as asyncEach} from "async"


function addTagToImage(tag, imageId, callback, errCallback){
  Tag.findOrCreate({
    where: { name: tag.toLowerCase()
    },
    defaults: {name: tag.toLowerCase() }}).then((data) => {
      return data
    }).then((data2) => {
      return ImageTag.create({ImageId: imageId, TagId: data2[0].dataValues.id})
    }).then(() => {
      callback(true)
    }).catch(() => {
      errCallback(null)
    })
}

function addTag(name, callback, errCallback){
  Tag.create({name: name.toLowerCase()}).then((data) => {
    return callback(data)
  }).catch((err) => {
    return errCallback(err)
  })
}

function getTagById(id, callback, errCallback){
  Tag.findOne({where: { id }}).then((data) => {
    const {id, name, createdAt, updatedAt} = data
    return callback({id, name, createdAt, updatedAt})
  }).catch((err) => errCallback(err))
}

function getTagsAssiociateWithImageId(ImageId){
  return ImageTag.findAll({where: {ImageId}})
    .then((data) => data.map((obj) => obj.TagId))
    .then((lstTags) => {
      console.log(lstTags)
      return Tag.findAll({where: {id: lstTags}})
    })
    .then((data) => data)
}

export default {addTagToImage, addTag, getTagById, getTagsAssiociateWithImageId}
