import { UserAccount } from "../model"
import { generateHashPassword, verifyPassword } from "../../lib/crypto/crypto"

function getUserByUsernameExcludeAllExceptPassword(username, callback, errCallback){
  UserAccount.findOne({
    where: { username },
    attributes: ["username", "hashPassword"]
  }).then((result) => {
    const {username, hashPassword} = result
    return callback({username, hashPassword})

  }).catch((error) => {
    throw error
    // return errCallback(error)
  })
}

// Tested
function getUserByUsernameExcludePassword(username, callback, errCallback){
  UserAccount.findOne({
    where: {username},
    attributes: ["id", "username", "firstname", "lastname"]
  }).then((result) => {
    const {id, username, firstname, lastname} = result
    return callback({id, username, firstname, lastname})

  }).catch((err) => {
    return errCallback(err)
  })
}

// Tested
function getUserByUsernameExcludeAllExceptId(username, callback, errCallback){
  UserAccount.findOne({
    where: {username},
    attributes: ["id", "username"]
  }).then((result) => {
    const {id, username} = result
    return callback({id, username})
  }).catch((err) => { return errCallback(err) })
}

function updatePasswordByUsername(username, password, callback, errCallback){
  isUserExist(username, (exist) => {
    if (exist){
      generateHashPassword(password).then((hashPassword) => {
        UserAccount.update({hashPassword}, {where: {username}}).then((data) => {

        })
      })

    } else {
      return callback()
    }
  })
}

// Tested
function createUser(username, password, firstname, lastname, callback, errCallback){
  isUserExist(username, (isExist) => {
    if (!isExist) {
      generateHashPassword(password).then((hashPassword) => {
        UserAccount.create({username, hashPassword, firstname, lastname}).then((user) => {
          const {id, username, firstname, lastname} = user
          return callback({id, username, firstname, lastname})
        })
      }).catch((err) => { return errCallback(err) })
    } else {
      return callback(null)
    }
  })
}

// Tested
function isUserExist(username, callback, errCallback){
  UserAccount.count({where: {username}}).then((count) => {
    if (count > 0) return callback(true)
    else return callback(false)
  }).catch((err) => {errCallback(err)})
}


export default {
  getUserByUsernameExcludeAllExceptPassword, getUserByUsernameExcludePassword, updatePasswordByUsername, createUser,
  getUserByUsernameExcludeAllExceptId, isUserExist
}
