import { Album, ImageDetails} from "../model"
// import { Tag as TagQuery } from "./index"
// import { eachOf as asyncEach } from "async"

// Tested
function createAlbum(userid, albumName, isPublic, callback, errCallback){
  Album.create({name: albumName, isPublic, UserAccountId: userid}).then((data) => {
    return callback(data)
  }).catch((err) => {
    return errCallback(err)
  })
}

// Tested
function isAlbumExist(albumid, callback, errCallback){
  Album.count({where: {id: albumid}}).then((count) => {
    if (count > 0) return callback(true)
    return callback(false)
  }).catch((err) => {
    return errCallback(err)
  })
}

function addImageToAlbum(albumid, urlLocation, originalname, callback, errCallback){
  isAlbumExist(albumid, (exist) => {
    if (exist) {
      ImageDetails.create({ImageLocation: urlLocation, AlbumId: albumid, originalName: originalname}).then((data) => {
        const d = {albumId: albumid, url: urlLocation, originalname: originalname, imageId: data.id}
        return callback(d)
      }).catch((err) => {
        errCallback(err)
      })
    }else{
      return callback(null)
    }

  },(err) => {
    return errCallback(err)
  })
}

function getImagesFromAlbum(albumid, callback, errCallback){
  ImageDetails.findAll({where: { AlbumId: albumid }})
    .then((data) =>
      callback(data))
    .catch((err) => errCallback(err))
}



function getAllAlbums(callback, errCallback){
  Album.findAll().then((data) => {
    return callback(data)
  }).catch((err) => { return errCallback(err)})
}

function getAllPublicAlbums(callback, errCallback){
  Album.findAll({where: {isPublic: true}}).then((data) => {
    return callback(data)
  }).catch((err) => {
    return errCallback(err)
  })
}

function getAlbumName(albumid, callback, errCallback){
  Album.findOne({where: {id: albumid}, attributes: ["id", "name"]}).then((data) => {
    return callback(data)
  }).catch((err) => { return errCallback(err) })
}

export default {
  createAlbum, isAlbumExist, addImageToAlbum, getImagesFromAlbum,
  getAllPublicAlbums, getAllAlbums, getAlbumName }
