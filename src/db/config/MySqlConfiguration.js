import Sequelize from "sequelize"

const db = new Sequelize("webapp-ImageAlbum", "root", "123456", {
  define: { engine: "InnoDB" },
  host: "localhost",
  dialect: "mysql",
  pool: {
    max: 1000,
    min: 0,
    idle: 2000
  }
})

export { Sequelize, db }
