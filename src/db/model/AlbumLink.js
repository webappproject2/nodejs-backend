import { Sequelize, db } from "../config/MySqlConfiguration"

const model = {
  id:{
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  link: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
    notEmpty: true
  }

}

const config = {
  timestamps: true,
}

const name = "AlbumLink"

export default db.define(name, model, config)
