import {Sequelize, db} from "../config/MySqlConfiguration"

const model = {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  username: {
    type: Sequelize.STRING(20),
    allowNull: false,
    unique: true,
    notEmpty: true,
    validate: {
      len: [5,20],
      notEmpty: true
    }
  },
  hashPassword: {
    type: Sequelize.STRING(60),
    allowNull: false,
    notEmpty: true
  },
  firstname: {
    type: Sequelize.STRING(30),
    allowNull: false,
    notEmpty: true,
    validate: {
      isAlpha: true,
      notEmpty: true
    }
  },
  lastname: {
    type: Sequelize.STRING(30),
    allowNull: false,
    notEmpty: true,
    validate: {
      isAlpha: true,
      notEmpty: true
    }
  }
}

const config = {
  timestamps: true,
}

const name = "UserAccount"



export default db.define(name, model, config)
