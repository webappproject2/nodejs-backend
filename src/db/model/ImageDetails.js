import { Sequelize, db } from "../config/MySqlConfiguration"

const model = {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  ImageLocation: {
    type: Sequelize.STRING
  },
  originalName: {
    type: Sequelize.STRING
  }
}

const config = {
  timestamps: true
}
const name = "ImageDetails"

export default db.define(name, model, config)
