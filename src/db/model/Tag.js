import {Sequelize, db} from "../config/MySqlConfiguration"

const model = {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: Sequelize.STRING
  }
}

const config = {
}

const name = "Tag"


export default db.define(name, model, config)
