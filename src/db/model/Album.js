import { Sequelize, db } from "../config/MySqlConfiguration"

// TODO: Add Field - AddedByWhichUser
const model = {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    notEmpty: true
  },
  isPublic: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false
  }
}

const config = {
  timestamps: true
}

const name = "Album"


export default db.define(name, model, config)
