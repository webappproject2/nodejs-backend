import { Sequelize, db } from "../config/MySqlConfiguration"

const model = {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  }
}

const config = {
  timestamps: true,
  updatedAt: false
}

const name = "ImageTag"


export default db.define(name, model, config)
