import Album from "./Album"
import AlbumLink from "./AlbumLink"
import ImageDetails from "./ImageDetails"
import ImageTag from "./ImageTag"
import Tag from "./Tag"
import UserAccount from "./UserAccount"

import {Sequelize, db} from "../config/MySqlConfiguration"
// import eachOf from "async/eachOf"
// import fs from "fs"
// import path from "path"

UserAccount.hasMany(Album, {as: { singular: "album", plural: "albums" }})
Album.hasMany(ImageDetails, {as: { singular: "image", plural: "images" }})
AlbumLink.belongsTo(Album, {as: "album"})

// User.belongsToMany(Role, { as: 'Roles', through: { model: UserRole, unique: false }, foreignKey: 'user_id' });
// Role.belongsToMany(User, { as: 'Users', through: { model: UserRole, unique: false }, foreignKey: 'role_id' });

ImageDetails.belongsToMany(Tag, { as: { singular: "tag", plural: "tags" }, through: { model: ImageTag, unique: false }, foreignKey: "ImageId"})
Tag.belongsToMany(ImageDetails, { as: { singular: "image", plural: "images" }, through: { model: ImageTag, unique: false },  foreignKey: "TagId"})

// Album.sync()
// AlbumLink.sync()
// ImageDetails.sync()
// ImageTag.sync()
// Tag.sync()
// UserAccount.sync()

UserAccount.sync().then(() => {
  UserAccount.findOrCreate({where : {username: "admin"}, defaults: {
    username: "admin",
    hashPassword: "$2a$09$aEgQct/rqsY5MBl/7l6VAu71Vh7Zy4P7mZ.LdMJxBWk.B9cxYqWE6",
    firstname: "Admin",
    lastname: "Admin"
  }})
  return Album.sync()
}).then(() => {
  return AlbumLink.sync()
}).then(() => {
  return ImageDetails.sync()
}).then(() => {
  return Tag.sync()
}).then(() => {
  return ImageTag.sync()
})




export {Album, ImageDetails, ImageTag, Tag, UserAccount, Sequelize, db}
