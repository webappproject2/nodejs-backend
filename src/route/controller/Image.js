import {Router} from "express"
import { isAuthenticated } from "../middleware/isAuthenticated"
import { Tag } from "../../db/query/"

const router = Router()


router.get("/:imageId/tags", (req, res) => {
  const {imageId} = req.params


  Tag.getTagsAssiociateWithImageId(imageId).then((data) => {
    return res.json(data)
  })
  // return res.sendStatus(200)

})


router.post("/tag", isAuthenticated,(req,res) => {
  const {tagName, imageId} = req.body

  if (tagName ===  null || tagName === undefined) {
    return res.sendStatus(400)
  }
  if (imageId ===  null || imageId === undefined) {
    return res.sendStatus(400)
  }
  Tag.addTagToImage(tagName, imageId, (data) => {
    return res.send(data)
  }, () => {
    return res.sendStatus(500)
  })
})



export default router
