import {Router} from "express"
import { isAuthenticated } from "../middleware/isAuthenticated"
import { User } from "../../db/query/"
const router = Router()

router.get("/whoami", isAuthenticated, (req, res) => {
  res.json(req.user)
})

router.post("/create", isAuthenticated,(req, res) => {
  const {username, password, firstname, lastname} = req.body
  if (!username || !password || !firstname || !lastname) {
    return res.status(400).json({
      username: {
        req: true,
        type: "STRING"
      },
      "password": {
        req: true,
        type:"STRING"
      },
      "firstname": {req: true, type: "STRING"},
      "lastname": {req: true, type: "STRING"}
    })
  }

  User.createUser(username, password, firstname, lastname, (data) => {
    if (data) {
      return res.status(201).json(data)
    }else {
      return res.sendStatus(409)
    }
  }, (err) => {
    return res.sendStatus(500)
  })
})

export default router
