import {Router} from "express"
import { isAuthenticated } from "../middleware/isAuthenticated"
import { Album, Tag } from "../../db/query/"
import {each as asyncEach} from "async"
import multer from "multer"
const upload = multer({ dest: "public/uploads/" })

const router = Router()

router.get("/", isAuthenticated, (req, res) => {
  Album.getAllAlbums((data) => {
    return res.json(data)
  }, (err) => {
    return res.sendStatus(500)
  })
})

router.get("/public", (req, res) => {
  Album.getAllPublicAlbums((data) => {
    return res.json(data)
  }, () => {
    return res.sendStatus(500)
  })
})

router.get("/:albumid", (req, res) => {
  const {albumid} = req.params
  if (albumid === undefined || albumid === null) return res.sendStatus(400)

  Album.getImagesFromAlbum(albumid, (images) => {
    Album.getAlbumName(albumid, (album) => {
      const r = {album, images}
      return res.json(r)
    }, (err) => {
      return res.status(500).json(err)
    })
  }, (err) => {
    throw err
    // return res.status(500).json(err)
  })
})

router.post("/:albumid/upload" , isAuthenticated,upload.single("photos"), (req, res) => {
  const {albumid} = req.params
  const {filename, originalname} = req.file
  let tags = undefined


  // ESlint is giving me Unexpected token try
  try {
    tags = JSON.parse(req.body.tags)
  } catch(err) {
    return res.sendStatus(400)
  }


  if (albumid === undefined || albumid === null) return res.sendStatus(400)
  Album.isAlbumExist(albumid, (exist) => {
    if (!exist) return res.sendStatus(400)
    else {
      Album.addImageToAlbum(albumid, `/uploads/${filename}`, originalname, (data) => {
        const {albumId, imageId} = data

        asyncEach(tags, (item, callback) => {
          Tag.addTagToImage(item, imageId, () => {
            callback()
          })
        }, (err) => {
          if (err) return res.status(500)

          const {filename, originalname} = req.file
          return res.status(201).json({url: `/uploads/${filename}`, originalname, imageId, albumId })
        })
      }, (err) => {
        return res.status(500).json(err)
      })
    }
  })
})

router.post("/create", isAuthenticated, (req, res) => {

  const {id} = req.user
  const {name, isPublic} = req.body

  if (!name || (isPublic === undefined) || isPublic === null) return res.sendStatus(400)
  Album.createAlbum(id, name, isPublic, (s) => {
    return res.status(201).json(s)
  }, (err) => {
    return res.this.status(500).json(err)
  })
})

export default router
