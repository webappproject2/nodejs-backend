import { Router } from "express"
import passport from "passport"
import { isAuthenticated } from "../middleware/isAuthenticated"

const router = Router()

router.post("/login", passport.authenticate("local-signin"), (req, res) => {
  res.status(200).json({success: true})

})

router.delete("/logout",isAuthenticated,(req, res) => {
  req.logOut()
  req.session.destroy()
  res.sendStatus(200)
})

export default router
